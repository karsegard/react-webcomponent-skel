"use strict";
var React = require("react");
var ReactDOM = require("react-dom");
var PropTypes = require("prop-types");
function _interopDefaultLegacy(e) {
  return e && typeof e === "object" && "default" in e ? e : { "default": e };
}
var React__default = /* @__PURE__ */ _interopDefaultLegacy(React);
var ReactDOM__default = /* @__PURE__ */ _interopDefaultLegacy(ReactDOM);
var PropTypes__default = /* @__PURE__ */ _interopDefaultLegacy(PropTypes);
var reactComponentSymbol = Symbol.for("r2wc.reactComponent");
var renderSymbol = Symbol.for("r2wc.reactRender");
var shouldRenderSymbol = Symbol.for("r2wc.shouldRender");
var define = {
  expando: function(receiver, key, value) {
    Object.defineProperty(receiver, key, {
      enumerable: true,
      get: function() {
        return value;
      },
      set: function(newValue) {
        value = newValue;
        this[renderSymbol]();
      }
    });
    receiver[renderSymbol]();
  }
};
function reactToWebComponent(ReactComponent, React2, ReactDOM2, options = {}) {
  var renderAddedProperties = { isConnected: "isConnected" in HTMLElement.prototype };
  var rendering = false;
  var WebComponent = function() {
    var self = Reflect.construct(HTMLElement, arguments, this.constructor);
    if (options.shadow) {
      self.attachShadow({ mode: "open" });
    }
    return self;
  };
  var targetPrototype = Object.create(HTMLElement.prototype);
  targetPrototype.constructor = WebComponent;
  var proxyPrototype = new Proxy(targetPrototype, {
    has: function(target, key) {
      return key in ReactComponent.propTypes || key in targetPrototype;
    },
    set: function(target, key, value, receiver) {
      if (rendering) {
        renderAddedProperties[key] = true;
      }
      if (typeof key === "symbol" || renderAddedProperties[key] || key in target) {
        return Reflect.set(target, key, value, receiver);
      } else {
        define.expando(receiver, key, value);
      }
      return true;
    },
    getOwnPropertyDescriptor: function(target, key) {
      var own = Reflect.getOwnPropertyDescriptor(target, key);
      if (own) {
        return own;
      }
      if (key in ReactComponent.propTypes) {
        return { configurable: true, enumerable: true, writable: true, value: void 0 };
      }
    }
  });
  WebComponent.prototype = proxyPrototype;
  targetPrototype.connectedCallback = function() {
    this[shouldRenderSymbol] = true;
    this[renderSymbol]();
  };
  targetPrototype[renderSymbol] = function() {
    if (this[shouldRenderSymbol] === true) {
      var data = {};
      Object.keys(this).forEach(function(key) {
        if (renderAddedProperties[key] !== false) {
          data[key] = this[key];
        }
      }, this);
      rendering = true;
      const container = options.shadow ? this.shadowRoot : this;
      this[reactComponentSymbol] = ReactDOM2.render(React2.createElement(ReactComponent, data), container);
      rendering = false;
    }
  };
  if (ReactComponent.propTypes) {
    WebComponent.observedAttributes = Object.keys(ReactComponent.propTypes);
    targetPrototype.attributeChangedCallback = function(name, oldValue, newValue) {
      this[name] = newValue;
    };
  }
  return WebComponent;
}
const Index = ({ bits = 2048 }) => {
  React.useState("");
  return /* @__PURE__ */ React__default["default"].createElement(React__default["default"].Fragment, null, "Super Component");
};
Index.propTypes = {
  bits: PropTypes__default["default"].string,
  passphrase: PropTypes__default["default"].string
};
customElements.define("my-component", reactToWebComponent(Index, React__default["default"], ReactDOM__default["default"]));
